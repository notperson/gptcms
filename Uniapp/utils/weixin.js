import jweixin from "weixin-js-sdk";

export default {
  isWechat: function () {
    var ua = window.navigator.userAgent.toLowerCase();
    if (ua.match(/micromessenger/i) == "micromessenger") return true;
    return false;
  },

  initJssdk(callback) {
    window.$vueApp.$u.api
      .configShareSdk({ url: window.location.href.split("#")[0] })
      .then((res) => {
        if (res.status === "error") {
          return;
        }
        jweixin.config({
          debug: res.data.debug || false,
          appId: res.data.appId,
          timestamp: res.data.timestamp,
          nonceStr: res.data.nonceStr,
          signature: res.data.signature,
          jsApiList: res.data.jsApiList || [
            "updateAppMessageShareData",
            "updateTimelineShareData",
            "onMenuShareTimeline",
            "onMenuShareAppMessage",
            "onMenuShareQQ",
            "onMenuShareWeibo",
            "onMenuShareQZone",
          ],
        });
        if (callback) {
          callback(res.data);
        }
      });
  },

  share(data, callback) {
    if (!this.isWechat()) {
      return;
    }
    this.initJssdk(function (res) {
      jweixin.ready(function () {
        jweixin.onMenuShareAppMessage({
          title: data.name,
          desc: data.desc,
          link: data.link,
          imgUrl: data.pics,
          type: "link",
          success: function (rs) {
            callback(rs);
          },
        });
      });
    });
  },
  //在需要定位页面调用
  getlocation: function (callback) {
    if (!this.isWechat()) {
      return;
    }
    this.initJssdk(function (res) {
      jweixin.ready(function () {
        jweixin.getLocation({
          type: "gcj02", // 默认为wgs84的gps坐标，如果要返回直接给openLocation用的火星坐标，可传入'gcj02'
          success: function (res) {
            callback(res);
          },
          fail: function (res) {
            console.log(res);
          },
          complete: function (res) {
            console.log(res);
          },
        });
      });
    });
  },
  openlocation: function (data, callback) {
    //打开位置
    if (!this.isWechat()) {
      return;
    }
    this.initJssdk(function (res) {
      jweixin.ready(function () {
        jweixin.openLocation({
          //根据传入的坐标打开地图
          latitude: data.latitude,
          longitude: data.longitude,
        });
      });
    });
  },
  chooseImage: function (callback) {
    //选择图片
    if (!this.isWechat()) {
      return;
    }
    this.initJssdk(function (res) {
      jweixin.ready(function () {
        jweixin.chooseImage({
          count: 1,
          sizeType: ["compressed"],
          sourceType: ["album"],
          success: function (rs) {
            callback(rs);
          },
        });
      });
    });
  },
  wxpay: function (data, callback) {
    if (!this.isWechat()) {
      return;
    }
    this.initJssdk(function (res) {
      jweixin.ready(function () {
        jweixin.chooseWXPay({
          timestamp: data.timestamp,
          nonceStr: data.nonceStr,
          package: data.package,
          signType: data.signType,
          paySign: data.paySign,
          success: function (res) {
            callback(res);
          },
          fail: function (res) {
            callback(res);
          },
          complete: function (res) {},
        });
      });
    });
  },
};

// #endif
